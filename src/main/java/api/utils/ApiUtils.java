package api.utils;

import java.util.List;
import java.util.Optional;

import api.models.Cidade;
import api.models.Cliente;
import api.models.Pet;
import api.repositories.CidadeRepository;
import api.repositories.ClienteRepository;
import api.repositories.PetRepository;

public class ApiUtils {

	public static Cidade buscarCidadePorId(CidadeRepository repository, Long id) {
		Optional<Cidade> op = repository.findById(id);
		if (op.isPresent()) {
			return op.get();
		}
		return null;
	}
	
	public static Cliente buscarClientePorId(ClienteRepository repository, Long id) {
		Optional<Cliente> op = repository.findById(id);
		if (op.isPresent()) {
			return op.get();
		}
		return null;
	}
	
	public static Pet buscarPetPorId(PetRepository repository, Long id) {
		Optional<Pet> op = repository.findById(id);
		if (op.isPresent()) {
			return op.get();
		}
		return null;
	}

	public static List<Pet> buscarPetPorCliente(PetRepository repository, Cliente cliente) {
		return repository.findByCliente(cliente);
	}
}
