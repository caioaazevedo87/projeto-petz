package api.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.dto.PetDTO;
import api.excepction.ApiException;
import api.models.Cliente;
import api.models.Pet;
import api.repositories.ClienteRepository;
import api.repositories.PetRepository;
import api.utils.ApiUtils;

@Component
@Path("/pet")
public class PetResource {

	private static final String LISTA_VAZIA = "Nenhum pet cadastrado!";
	private static final String PET_NAO_LOCALIZADO = "pET não localizado!";
	private static final String ATRIBUTO_NAO_PREENCHIDO = "Campos obrigatórios: NOME, ESPECIE, CLIENTE";

	private PetRepository petRepository;
	private ClienteRepository clienteRepository;

	@Autowired
	public PetResource(PetRepository petRepository, ClienteRepository clienteRepository) {
		super();
		this.petRepository = petRepository;
		this.clienteRepository = clienteRepository;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() {
		List<Pet> lista = petRepository.findAll();
		if (lista.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(LISTA_VAZIA)).build();
		}
		return Response.status(Status.OK).entity(lista).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Long id) {
		try {
			Pet pet = localizarPet(id);
			return Response.status(Status.OK).entity(pet).build();
		} catch (ApiException ae) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
		}
	}
	
	@GET
	@Path("/{idCliente}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findByIdCliente(@PathParam("idCliente") Long id) {
		try {
			Cliente cli = ApiUtils.buscarClientePorId(clienteRepository, id);
			List<Pet> lista = ApiUtils.buscarPetPorCliente(petRepository, cli);
			return Response.status(Status.OK).entity(lista).build();
		} catch (ApiException ae) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response post(PetDTO dto) {
		try {
			validarPetIncluir(dto);
			Pet pet = new Pet();
			pet.setCliente(dto.getCliente());
			pet.setDataNascimento(dto.getDataNascimento());
			pet.setEspecie(dto.getEspecie());
			pet.setNome(dto.getNome());
			petRepository.save(pet);
			return Response.status(Status.CREATED).entity(pet).build();
		} catch (ApiException ae) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
		}
	}

	@PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(@PathParam("id") Long id, PetDTO dto) {
		try {
			Pet pet = localizarPet(id);
			if (dto.getCliente() != null) pet.setCliente(dto.getCliente());
			if (dto.getDataNascimento() != null) pet.setDataNascimento(dto.getDataNascimento());
			if (dto.getEspecie() != null) pet.setEspecie(dto.getEspecie());
			if (dto.getNome() != null) pet.setNome(dto.getNome());
			petRepository.save(pet);
			return Response.status(Status.CREATED).entity(pet).build();
		} catch (ApiException ae) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
		}
    }
	
	@DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
		Pet pet = localizarPet(id);
        try {
        	petRepository.delete(pet);
            return Response.status(Status.OK).build();
        } catch(Exception ex) {
        	return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
        }
    }
	
	private Pet localizarPet(Long id) {
		Pet pet = ApiUtils.buscarPetPorId(petRepository, id);
		if (pet == null) {
			throw new ApiException(PET_NAO_LOCALIZADO);
		}
		return pet;
	}
	
	private void validarPetIncluir(PetDTO dto) {
		if (dto.getNome() == null || dto.getCliente() == null || dto.getEspecie() == null) {
			throw new ApiException(ATRIBUTO_NAO_PREENCHIDO);
		}

	}

}