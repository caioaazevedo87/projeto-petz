package api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import api.models.Cliente;
import api.models.Pet;

@Repository
public interface PetRepository extends JpaRepository<Pet, Long>{
	
	public List<Pet> findByCliente(Cliente cliente);
	
}
